from django.contrib.auth.models import User
from django.db.models import QuerySet
from django.http import Http404
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet, ModelViewSet

from alphavantage_interface.celery_app import app
from .models import ExchangeRate
from .permissions import UserSpecificPermission
from .serializers import UserSerializer, ExchangeRateSerializer


class UserViewSet(ModelViewSet):
    queryset = User.objects.order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [UserSpecificPermission]

    def get_queryset(self):
        assert self.queryset is not None, (
                "'%s' should either include a `queryset` attribute, "
                "or override the `get_queryset()` method."
                % self.__class__.__name__
        )

        queryset = self.queryset
        if isinstance(queryset, QuerySet):
            # Ensure queryset is re-evaluated on each request.
            if self.request.user.is_staff:
                queryset = queryset.all()
            else:
                queryset = queryset.filter(id=self.request.user.id).all()
        return queryset


class ExchangeRateViewSet(ViewSet):
    permission_classes = [IsAuthenticated]

    def list(self, request):
        try:
            record = ExchangeRate.objects.latest('created_at')
        except ExchangeRate.DoesNotExist:
            raise Http404
        else:
            data = ExchangeRateSerializer(record, context={'request': request}, many=False).data
            return Response(data, status=status.HTTP_200_OK)

    def create(self, request):
        app.send_task(name='get_exchange_rate')
        return Response({}, status=status.HTTP_201_CREATED)
