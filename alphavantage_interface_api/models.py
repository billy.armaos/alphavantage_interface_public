from django.db import models


class ExchangeRate(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    exchange_rate = models.FloatField()

    def __str__(self):
        return f'Exchange rate at {self.created_at}: {self.exchange_rate}.'

    class Meta:
        ordering = ['-created_at']
