import requests

from alphavantage_interface.settings import ALPHAVANTAGE_API_KEY
from alphavantage_interface.celery_app import app
from alphavantage_interface_api.models import ExchangeRate


@app.task(bind=True, default_retry_delay=10, max_retries=3, name='get_exchange_rate')
def get_exchange_rate(self, **kwargs):
    url = f'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=BTC&to_currency=USD&apikey={ALPHAVANTAGE_API_KEY}'
    alphavantage_response = requests.get(url)
    alphavantage_response_json = alphavantage_response.json()
    exchange_rate = alphavantage_response_json['Realtime Currency Exchange Rate']['5. Exchange Rate']
    ExchangeRate.objects.create(exchange_rate=exchange_rate)
    return 'Sent request to retrieve latest exchange rate'
