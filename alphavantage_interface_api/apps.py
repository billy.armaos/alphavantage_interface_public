from django.apps import AppConfig


class AlphavantageInterfaceApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'alphavantage_interface_api'
