import os

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'alphavantage_interface.settings')
app = Celery('alphavantage_interface')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
