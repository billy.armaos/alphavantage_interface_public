from django.urls import reverse


def test_get_200(exchange_rate_fixture, user, api_client):
    api_client.force_authenticate(user)
    response = api_client.get(reverse('ExchangeRate-list'))
    assert response.status_code == 200
    response_json = response.json()
    assert 'id' in response_json
    assert 'created_at' in response_json
    assert 'exchange_rate' in response_json


def test_get_401(exchange_rate_fixture, api_client):
    response = api_client.get(reverse('ExchangeRate-list'))
    assert response.status_code == 401


def test_post_200(user, api_client, mocker):
    mock = mocker.patch('alphavantage_interface.celery_app.app.send_task')
    api_client.force_authenticate(user)
    response = api_client.post(reverse('ExchangeRate-list'))
    assert response.status_code == 201
    assert mock.call_count == 1
    assert response.json() == {}


def test_post_401(api_client):
    response = api_client.post(reverse('ExchangeRate-list'))
    assert response.status_code == 401
