import pytest
from django.conf import settings
from pytest_factoryboy import register
from rest_framework.test import APIClient

from tests.factories import ExchangeRateFactory, UserFactory

# disable throttling for tests
settings.REST_FRAMEWORK['DEFAULT_THROTTLE_CLASSES'] = []

register(UserFactory)
register(ExchangeRateFactory)


@pytest.fixture(name='exchange_rate_fixture')
def fixture_exchange_rate(exchange_rate_factory):
    exchange_rate = exchange_rate_factory.create()
    return exchange_rate


# make DRF APIClient a fixture
@pytest.fixture
def api_client():
    return APIClient()


# enable database access to all fixtures by default
@pytest.fixture(autouse=True)
def enable_db_access_for_all_tests(db):
    pass
