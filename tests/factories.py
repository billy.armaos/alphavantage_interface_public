import factory
from django.contrib.auth import get_user_model

from alphavantage_interface_api.models import ExchangeRate


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = get_user_model()

    username = factory.Faker('first_name')
    # this is a post-generation hook that sets password if provided
    password = factory.PostGeneration(lambda user, create, extracted: user.set_password(extracted))


class ExchangeRateFactory(factory.django.DjangoModelFactory):
    exchange_rate = factory.Faker('pyfloat')

    class Meta:
        model = ExchangeRate
