FROM nginx

COPY ./nginx.conf /etc/nginx/conf.d/default.conf
COPY /alphavantage_interface/static /static/static
